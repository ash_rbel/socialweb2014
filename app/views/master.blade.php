<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Drive Together</title>

    <!-- for Facebook -->          
<!--     <meta property="og:title" content="Drive together" />
    <meta property="og:type" content="website" />
    <meta property="og:image" content="images/logo_dark.png" />
    <meta property="og:url" content="http://social.dev" />
    <meta property="og:description" content="Somethign something" />
 -->
    <!-- Styles -->

    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="css/londinium-theme.min.css" rel="stylesheet" type="text/css">
    <link href="css/styles.min.css" rel="stylesheet" type="text/css">
    <link href="css/icons.min.css" rel="stylesheet" type="text/css">

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">

<style>
.btn-block.h6 {
    margin-top: 0;
    margin-bottom: 0;
}
    #map-canvas {
        height: 100%;
    }

    .page-content {
        margin: 0 0 0 240px;
    }

    .sidebar-hidden .page-content {
        margin: 0 !important;
    }

    @media (max-width: 768px) {
        .page-content {
            margin: 0 !important;
        } 
    }

    @media (max-width: 991px) {
        .page-content {
            margin: 0 !important;
        } 
    }   

    .user-menu>a img {
        height: 48px;
    }

    .panel-block {
        border-radius: 0;
    }

    .message-list {
        max-height: 300px;
        overflow: auto;
    }

    .btn-fb, .btn-fb:hover, .btn-fb:focus {
        background-color: #3b5998;
        color: #fff;
        border: none; 
    }

    .btn-tw, .btn-tw:hover, .btn-tw:focus {
        background-color: #55acee;
        color: #fff;
        border: none; 
    }    

    .btn-share {
        width: 150px;
    }

    .btn-share+.btn-share {
        margin-top: 5px;
    }

    .panel {
        margin-bottom: 0;
    }

    .panel-transparent {
        background: none;
        color: white;
    }

    .twitter-message{
        font-size:200%;
        font-style: italic;
        font-color#ccc;
    }

    .twitter-message img {
        width: 50px;
        height: 50px;
    }

    .chat-member+.chat-member {
        padding-top: 10px;
        border-top: solid 1px black;
        margin-top: 10px;
    }    

</style>

    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>
    <script type="text/javascript" src="http://cdn.jsdelivr.net/jquery.animate-enhanced/1.07/jquery.animate-enhanced.min.js"></script>
    

    <script type="text/javascript" src="js/plugins/charts/sparkline.min.js"></script>
    
    <!-- Forms -->
    <script type="text/javascript" src="js/plugins/forms/uniform.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/select2.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/inputmask.js"></script>
    <script type="text/javascript" src="js/plugins/forms/autosize.js"></script>
    <script type="text/javascript" src="js/plugins/forms/inputlimit.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/listbox.js"></script>
    <script type="text/javascript" src="js/plugins/forms/multiselect.js"></script>
    <script type="text/javascript" src="js/plugins/forms/validate.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/tags.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/switch.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/uploader/plupload.full.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/uploader/plupload.queue.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/wysihtml5/wysihtml5.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/wysihtml5/toolbar.js"></script>
    
    <!-- Interface -->
    <script type="text/javascript" src="js/plugins/interface/daterangepicker.js"></script>
    <script type="text/javascript" src="js/plugins/interface/fancybox.min.js"></script>
    <script type="text/javascript" src="js/plugins/interface/moment.js"></script>
    <script type="text/javascript" src="js/plugins/interface/jgrowl.min.js"></script>
    <script type="text/javascript" src="js/plugins/interface/datatables.min.js"></script>
    <script type="text/javascript" src="js/plugins/interface/colorpicker.js"></script>
    <script type="text/javascript" src="js/plugins/interface/fullcalendar.min.js"></script>
    <script type="text/javascript" src="js/plugins/interface/timepicker.min.js"></script>
    <script type="text/javascript" src="js/plugins/interface/collapsible.min.js"></script>
    
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/application.js"></script>

    <script type="text/javascript">
        if (window.location.hash && window.location.hash == '#_=_') {
            if (window.history && history.pushState) {
                window.history.pushState("", document.title, window.location.pathname);
            } else {
                // Prevent scrolling by storing the page's current scroll offset
                var scroll = {
                    top: document.body.scrollTop,
                    left: document.body.scrollLeft
                };
                window.location.hash = '';
                // Restore the scroll offset, should be flicker free
                document.body.scrollTop = scroll.top;
                document.body.scrollLeft = scroll.left;
            }
        }
    </script>

    @yield('scripts')           

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    @yield('styles')            
</head>

<body class="sidebar-wide">
    <!-- Navbar -->
<div class="navbar navbar-inverse" role="navigation">
    <div class="navbar-header"><a class="navbar-brand" href="#">Drive Together</a><a class="sidebar-toggle"><i class="icon-paragraph-justify2"></i></a>
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar">
            <span class="sr-only">Toggle navigation</span><i class="icon-paragraph-justify2"></i>
        </button>
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-icons"><span class="sr-only">Toggle navbar</span><i class="icon-grid3"></i></button>
    </div>


        <ul class="nav navbar-nav navbar-right collapse" id="navbar-icons">
            <li>
                <a href="logout">
                    <span>Logout</span>
                </a>
            </li>
        </ul>    
</div>

    <!-- /navbar -->
    <!-- Content wrapper -->
    @yield('content')
    <!-- /Content wrapper -->


</body>

</html>




