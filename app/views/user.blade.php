@extends('master')

@section('content')

    <div class="login-wrapper">
            <div class="popup-header">
                <span class="text-semibold">Please sign in</span>
            </div>
            <div class="well">
                <a href="login/fb" type="button" class="btn btn-large btn-block btn-default btn-fb">Login with Facebook</a>
            </div>
    </div>

@stop