@extends('master')

@section('styles')

@stop

@section('scripts')

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDl8zutSHrmr_DL7OIpjX7dy2mStPyo620&sensor=true"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/list.js/1.1.1/list.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.6.0/underscore-min.js"></script>
<script type="text/javascript" src="js/map.js"></script>

@stop

@section('content')


<div class="page-container">
  <!-- Sidebar -->
  <div class="sidebar collapse">
    <div class="sidebar-content">
      <!-- User profile -->
    <div class="user-menu dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <img src="{{$user->profile->photo}}" alt="">
            <div class="user-info">{{$user->profile->name}}
            </div>

            <span id="user-work-lat" class="hide">{{$user->profile->work_lat}}</span>
            <span id="user-work-long" class="hide">{{$user->profile->work_long}}</span>

            <span id="user-home-lat" class="hide">{{$user->profile->home_lat}}</span>
            <span id="user-home-long" class="hide">{{$user->profile->home_long}}</span>            
        </a>
    </div>
      <!-- /user profile -->

		<!-- Input form -->
    <form role="form" action="#">
      <div class="panel panel-block panel-transparent">
        <div class="panel-body">
        	<div class="form-group">
        		<label>Home location:</label><br>
				    <span>{{$user->profile->home}}</span>
        <!-- <input type="text" class="form-control input-sm" value="{{$user->profile->home}}" /> -->
        	</div>

        	<div class="form-group">
        		<label>Work:</label><br>
            <span>{{$user->profile->work}}</span>
				<!-- <input type="text" class="form-control input-sm" value="{{$user->profile->work}}" /> -->
        	</div>



<!--           <div class="form-group">
            <label>Today's working hours:</label>
              <input type="text" class="form-control input-sm" id="hours-input" disabled/>
            </div>
            <div id="range-hours"></div>
 -->
          <div class="form-group">
            <label>Search radius around homeplace:</label>
              <input type="text" class="form-control input-sm" id="home-input" disabled/>
            </div>
          <div id="range-home"></div>

          <div class="form-group">
            <label>Search radius around workplace:</label>
              <input type="text" class="form-control input-sm" id="location-input" disabled/>
            </div>
          <div id="range-location"></div>
          
          <div class="form-group">
          	<label>Scope:</label>

                <select data-placeholder="Scope" id="scope" name="scope" class="select-full">
                  <option value="friends">Friends</option>
                  <option value="fof">Friends of Friends</option>
                </select>
          </div>

<!--             <div class="form-group">
            	<label>Transportation:</label>

                  <select data-placeholder="Scope" id="mode" name="scope" class="select-full" onchange="calcRoute();">
				      <option value="DRIVING">Driving</option>
				      <option value="WALKING">Walking</option>
				      <option value="BICYCLING">Bicycling</option>
				      <option value="TRANSIT">Transit</option>
                  </select>
            </div> -->

        </div>
      </div>
    </form>
		<!--  -->




    <div class="panel panel-block">
    	<div class="panel-heading">
       <h6>Friends <span id="friends-total">({{_Array::size($user->friends)}})</span></h6> 
      </div>

      <div id="friends-list">
        <ul class="message-list list">
              @foreach ($user->friends as $friend)

          <li>
              <div class="clearfix">
                  <div class="chat-member">
                      <a href="#"><img src="{{$friend->photo}}" alt=""></a>
                      <button type="button" class="btn btn-large pull-right btn-default btn-fb btn-message">F</button>
                      <span class="pull-left"><strong>{{$friend->name}}</strong></span>
                      <span class="pull-left btn-block h6"><small><strong>H:</strong> {{$friend->home}}</small></span>
                      <span class="pull-left btn-block h6"><small><strong>W:</strong> {{$friend->work}}</small></span>

                      <span class="hide home-lat">{{$friend->home_lat}}</span>
                      <span class="hide home-long">{{$friend->home_long}}</span>
                      <span class="hide home-distance">{{$friend->pivot->home_distance}}</span>

                      <span class="hide work-lat">{{$friend->work_lat}}</span>
                      <span class="hide work-long">{{$friend->work_long}}</span>
                      <span class="hide work-distance">{{$friend->pivot->work_distance}}</span>
                      <span class="hide photo">{{$friend->photo}}</span>
                  </div>
              </div>
          </li>
              @endforeach       

        </ul>
      </div>
    </div>

<!--     <div class="panel panel-block panel-transparent">
    	<div class="panel-body text-center">
    		<button type="button" class="btn btn-large btn-share btn-default btn-fb ">Share on Facebook</button>
    		<button type="button" class="btn btn-large btn-share btn-default btn-tw ">Share on Twitter</button>
    	</div>
    </div> -->

    </div>
  </div>
  <!-- /sidebar -->

  <div class="page-content full-height">


    <div id="map-canvas"/>
    </div>

</div>
</div>

 @stop