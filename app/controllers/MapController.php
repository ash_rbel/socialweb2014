<?php

use Treffynnon\Navigator\LatLong;
use Treffynnon\Navigator\Coordinate;
use Treffynnon\Navigator\Distance;
use Treffynnon\Navigator\Distance\Converter\MetreToKilometre;
use Treffynnon\Navigator\Distance\Calculator\GreatCircle;

use OAuth\OAuthToken;
use OAuth\OAuthConsumer;
use OAuth\OAuthSignatureMethod_HMAC_SHA1;
use OAuth\OAuthRequest;

class MapController extends BaseController {

	protected $facebook;
	protected $facebookSdk;
	protected $locations;

	function __construct() {
	    $this->facebookSdk = new Facebook(Config::get('facebook.app'));
	    $this->facebook = new SimpleFacebook($this->facebookSdk, Config::get('facebook.config'));

	    $this->locations = array();
	}

	public function home()
	{
	    $data = array();

	    if (Auth::check()) {
	        $data = Auth::user();
	    }

	    return View::make('user', array('data'=>$data));
	}

	public function facebookLogin() {
    	return Redirect::to($this->facebook->getLoginUrl());		
	}

	public function facebookLoginCallback() {
		$facebook = $this->facebook;

		$code = Input::get('code');
	 
	    if (strlen($code) == 0 || $facebook->getUser() == 0) {
	    	return Redirect::to('/')->with('message', 'There was an error communicating with Facebook');	
	    } 

		$user = User::whereUid($facebook->getUser())->first() ?: new User();
    	$user = $this->updateUser($user);

	    Auth::login($user);

	    return Redirect::to('/map');
	}


	protected function updateUser($user) {
		$facebookSdk = $this->facebookSdk;
		$facebook = $this->facebook;
		$locations = $this->locations;

	    $me = $facebook->api('/me');

	    $profile = $user->profile ?: new Profile();
	    $profile = $this->updateProfile($profile, $me);

	    $user->uid = $facebook->getUser();
	    $user->profile_id = $profile->id;
	    $user->email = $me['email'];
	    $user->access_token = $facebook->getAccessToken();
	    $user->save();

	    $profile = $user->profile()->save($profile);
	    $user = $profile->user;

	    $appFriends = $facebook->getAppUserFriends();
	    // foreach ($appFriends as $appFriend) {
	    // 	$friendProfile = User::whereUid($appFriend['uid'])->first()->profile;
	    //     $user->friends()->save($friendProfile, 
	    //     	array(
	    //     		'is_direct_friend' => true, 
	    //     		'work_distance' => $this->calculateDistance($profile, $friendProfile),
	    //     		'home_distance' => $this->calculateDistance($profile, $friendProfile)
     //    		)
    	// 	);
	    // }

	    $friends = _Array::filter($facebookSdk->api('me/friends?fields=id,username,name,location,work')['data'], function($friend) use($appFriends) {
	        return 
	        	// !_Array::contains(_Array::pluck($appFriends, 'id'), $friend['id']) 
	        	isset($friend['username']) 
	        	&& isset($friend['work']) 
	        	&& isset($friend['location']);
	    });

	    foreach ($friends as $friend) {
	        $friendProfile = Profile::whereUsername($friend['username'])->first() ?: new Profile();

	        $friendProfile = $this->updateProfile($friendProfile, $friend);
	        $friendProfile->save();

	        $workDistance = $this->calculateDistance(
        		array(
        			'lat' => $profile->work_lat,
        			'long' => $profile->work_long,
        		),
        		array(
        			'lat' => $friendProfile->work_lat,
        			'long' => $friendProfile->work_long,
        		)
			);

	        $homeDistance = $this->calculateDistance(
        		array(
        			'lat' => $profile->home_lat,
        			'long' => $profile->home_long,
        		),
        		array(
        			'lat' => $friendProfile->home_lat,
        			'long' => $friendProfile->home_long,
        		)
			);   

	        if (!$user->friends->contains($friendProfile->id)) {
	            $user->friends()->save($friendProfile, array('is_direct_friend' => true, 'work_distance' => $workDistance, 'home_distance' => $homeDistance));
	        } else {
	        	$friendProfile = $user->friends()->wherePivot('profile_id', $friendProfile->id)->wherePivot('user_id', $user->id)->first();
	        	$friendProfile->pivot->work_distance = $workDistance;
	        	$friendProfile->pivot->home_distance =  $homeDistance;

	        	$friendProfile->pivot->save();
	        }
	    }

	    return $user;
	}

	protected function updateProfile($profile, $me) {
		$facebook = $this->facebook;
		$facebookSdk = $this->facebookSdk;

        $profile->username = $me['username'];
        $profile->name = $me['name'];
        $profile->photo = 'https://graph.facebook.com/'.$me['username'].'/picture?type=large';

        if (is_null($profile->id) || $profile->home !== $me['location']['name']) {
        	list($profile->home, $profile->home_lat, $profile->home_long) = 
        		$this->updateLocation($profile, $me['location']);
        }

        if (is_null($profile->id) || $profile->work !== $me['work'][0]['employer']['name']) {
        	$employer = $facebookSdk->api($me['work'][0]['employer']['id']);

        	if (!isset($employer['location'])) {
        		$profile->work = $employer['name'];
        	} else {
	        	$workLocation = $employer['location'];
	        	list($profile->work, $profile->work_lat, $profile->work_long) = 
        			$this->updateLocation($profile, $workLocation);
        	}
        }    

        if (isset($profile->work_lat)) {
        	list($profile->work_start, $profile->work_stop) = $this->getOpeningHours($profile->work, array(
        		'lat' => $profile->work_lat,
        		'long' => $profile->work_long,
    		));
        }
	

        $profile->save();
		return $profile;	
	}

	protected function updateLocation($profile, $location) {
		$facebookSdk = $this->facebookSdk;
		$locations = $this->locations;
        
		if (isset($location['id'])) {
	        if (_Array::has($locations, $location['id'])) {
	            $location = $locations[$location['id']];
	        } else {
	            $location = $facebookSdk->api($location['id']);
	            $locations = _Array::append($locations, array($location['id'] => $location));
	        }

	        $locationName = $location['name'];
	        $location = $location['location'];
	    } else {
	    	$locationStreet = isset($location['street']) ? $location['street'] : '';
	    	$locationCity = isset($location['city']) ? $location['city'] : '';

	    	$locationName = $locationStreet . ', ' . $locationCity;
	    }

	    if (!isset($location['latitude'])) {
	    	return array($locationName, null, null);
    	}

        return array(
        	$locationName, 
        	$location['latitude'],
        	$location['longitude'],
    	);
	}

	protected function calculateDistance($location1, $location2) {
		if (is_null($location1['lat']) || is_null($location2['lat'])) {
			return -1;
		}

		$coord1 = new LatLong(
		    new Coordinate($location1['lat']),
		    new Coordinate($location1['long'])
		);

		$coord2 = new LatLong(
		    new Coordinate($location2['lat']),
		    new Coordinate($location2['long'])
		);

		$distance = new Distance($coord1, $coord2);
		$distance = $distance->get(new GreatCircle, new MetreToKilometre);

		return $distance;
	}

	protected function getOpeningHours($workName, $location) {
		$google_places = new GooglePlaces('AIzaSyBaiAFjW-wMn1crMZjin6wXkOXzFcGhJsY');
		
		$google_places->location = array($location['lat'], $location['long']);
		$google_places->radius   = 800;
		$google_places->name 	 = urlencode($workName);
		$results                 = $google_places->radarSearch();		

		if(empty($results['results'])) {
			return array(null, null);
		}

		$google_places->reference = $results['results'][0]['reference']; // Reference from search results
		$details                  = $google_places->details();	      

		if(!isset($details['result']['opening_hours'])) {
			return array(null, null);
		}

		$dayNumber = date('N', strtotime('today'));
		$closingTimes = _Array::filter($details['result']['opening_hours']['periods'], function($period) use ($dayNumber) {
			return isset($period['close']) && $period['close']['day'] == $dayNumber;
		});
		$closeTime = empty($closingTimes) ? null : _Array::first($closingTimes)['close']['time'];
		
		$openingTimes = _Array::filter($details['result']['opening_hours']['periods'], function($period) use ($dayNumber) {
			return isset($period['open']) && $period['open']['day'] == $dayNumber;
		});
		$openTime = empty($openingTimes) ? null : _Array::first($openingTimes)['open']['time'];



		return array($openTime, $closeTime);
	}

	public function logout()
	{
	    Auth::logout();
    	return Redirect::to('/');
	}

	public function map()
	{
	    if (Auth::check()) {
	        $user = Auth::user();
	    } else {
	        return Redirect::to('/');
	    }
	    
	    return View::make('map', array('user' => $user));		
	}
}