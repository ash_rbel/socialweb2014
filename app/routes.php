<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', 'MapController@home');

Route::get('login/fb', 'MapController@facebookLogin');
Route::get('login/fb/callback', 'MapController@facebookLoginCallback');
Route::get('logout', 'MapController@logout');

Route::get('map', 'MapController@map');
