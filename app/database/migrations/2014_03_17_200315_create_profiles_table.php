<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
	    Schema::create('profiles', function($table)
	    {
	        $table->increments('id');

	        $table->integer('user_id')->unsigned()->nullable();


	        $table->string('username');

	        $table->string('photo');
	        $table->string('name');

	        $table->string('home');
	        $table->float('home_lat');
	        $table->float('home_long');

	        $table->string('work');	
	        $table->float('work_lat')->nullable();
	        $table->float('work_long')->nullable();	  

	        $table->integer('work_start')->nullable();
	        $table->integer('work_stop')->nullable();	  

	              

	        $table->timestamps();
	    });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('profiles');
	}

}
