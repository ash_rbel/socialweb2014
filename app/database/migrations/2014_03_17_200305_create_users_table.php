<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
	    Schema::create('users', function($table)
	    {
	        $table->increments('id');

	        $table->integer('profile_id')->unsigned();
	        $table->biginteger('uid')->unsigned();

	        $table->string('email')->unique();	        
	        $table->string('access_token');
	        
	        $table->timestamps();
	    });	
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
