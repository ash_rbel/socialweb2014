<?php
// app/config/facebook.php

// Facebook app Config 
return array(
	'app' => array(
    	'appId' => '1476624392551273',
    	'secret' => 'fd381532b5ffe97f3f828157ee4e8ff3',
	),

	'config' => array(
        'redirect_uri' => url('/login/fb/callback'),
        'scope' => implode(', ', 
            array('email', 'user_about_me', 'user_work_history', 
                'friends_about_me', 'friends_work_history', 
                'friends_location', 'user_location'
            )
        ), 
	),

);