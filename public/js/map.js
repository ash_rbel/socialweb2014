$(function() {

    $(document).ready(function() {
        $.ajaxSetup({
            cache: true
        });
        $.getScript('//connect.facebook.net/en_UK/all.js', function() {
            FB.init({
                appId: '1476624392551273',
            });



            FB.getLoginStatus(function(status) {
                $(".btn-message").click(function(event) {
                    FB.ui({
                        method: 'send',
                        link: 'http://socialweb.rbelweb.com/public'
                    });
                });
            });
        });
    });

    //Initial load of page
    $(document).ready(sizeContent);

    //Every resize of window
    $(window).resize(sizeContent);

    //Dynamically assign height

    function sizeContent() {
        var newHeight = $(document).height() - $(".navbar").height() + "px";
        $(".full-height").css("height", newHeight);
    }

    $("#range-location").slider({
        range: true,
        min: 0,
        max: 50,
        values: [0, 8],
        slide: function(event, ui) {
            $("#location-input").val(ui.values[0] + "km - " + ui.values[1] + "km");
        },
        stop: function(event, ui) {
            filter();
        }
    });

    $("#location-input").val($("#range-location").slider("values", 0) + "km - " +
        $("#range-location").slider("values", 1) + "km");

    $("#range-home").slider({
        range: true,
        min: 0,
        max: 50,
        values: [0, 8],
        slide: function(event, ui) {
            $("#home-input").val(ui.values[0] + "km - " + ui.values[1] + "km");
        },
        stop: function(event, ui) {
            filter();
        }
    });

    $("#home-input").val($("#range-home").slider("values", 0) + "km - " +
        $("#range-home").slider("values", 1) + "km");

    function filter() {
        friendsList.filter();
        friendsList.filter(function(item) {
            var workDistance = item.values()['work-distance'];
            var withinWorkRange = ($("#range-location").slider("values", 1) >= workDistance) && ($("#range-location").slider("values", 0) <= workDistance);

            console.log(item.values()['home-distance']);
            var homeDistance = item.values()['home-distance'];
            var withinHomeRange = ($("#range-home").slider("values", 1) >= homeDistance) && ($("#range-home").slider("values", 0) <= homeDistance);

            return withinWorkRange && withinHomeRange;
        });
    }

    function createMap() {
        var mapOptions = {
            center: new google.maps.LatLng(52.370197, 4.890444),
            zoom: 14,
            mapTypeControlOptions: {
                mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'custom_style']
            },
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        var map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);

        var directionsService = new google.maps.DirectionsService();

        _.each(friendsList.matchingItems, function(friend) {
            var work = new google.maps.LatLng(friend.values()['work-lat'], friend.values()['work-long']);
            var home = new google.maps.LatLng(friend.values()['home-lat'], friend.values()['home-long'])

            var marker = new google.maps.Marker({
                position: work,
                map: map
            });

            var infowindow = new google.maps.InfoWindow({
                content: '<div class="twitter-message"><img src="' + friend.values()['photo'] + '" /></div>',
                disableAutoPan: false
            });

            var request = {
                origin: home,
                destination: work,
                travelMode: google.maps.TravelMode.DRIVING
            };

            var polylineOptionsActual = {
                strokeColor: '#' + Math.floor(Math.random() * 16777215).toString(16),
                strokeOpacity: 0.8,
                strokeWeight: 6
            };

            var directionsDisplay = new google.maps.DirectionsRenderer({
                suppressMarkers: true,
                polylineOptions: polylineOptionsActual
            });
            directionsDisplay.setMap(map);

            var respondToRoute = _.debounce(function(response, status){
                if (status == google.maps.DirectionsStatus.OK) {
                    directionsDisplay.setDirections(response);
                } else if (status == google.maps.DirectionsStatus.OVER_QUERY_LIMIT) {
                    directionsService.route(request, respondToRoute);
                }
            }, 800); 

            directionsService.route(request, respondToRoute);

            infowindow.open(map, marker);
        });


        console.log(userWork);

        //Adding Radius around Users work
        var radiusWork = {
            strokeColor: '#FF0000',
            strokeOpacity: 0.3,
            strokeWeight: 2,
            fillColor: '#FF0000',
            fillOpacity: 0.1,
            map: map,
            center: new google.maps.LatLng(userWork['lat'], userWork['long']),
            radius: ($("#range-location").slider("values", 1) - $("#range-location").slider("values", 0)) * 1000
        };

        var cityCircle = new google.maps.Circle(radiusWork);
    }


    var userWork = {
        'lat': parseFloat($('#user-work-lat').html()),
        'long': parseFloat($('#user-work-long').html()),
    }

    var userHome = {
        'lat': parseFloat($('#user-home-lat').html()),
        'long': parseFloat($('#user-home-long').html()),
    }

    var options = {
        valueNames: ['work-distance', 'work-lat', 'work-long', 'photo', 'home-lat', 'home-long', 'home-distance']
    };

    var friendsList = new List('friends-list', options);

    friendsList.on('filterComplete', function() {
        if (friendsList.filtered) { // prevent drawing map when removing filters
            $('#friends-total').html(friendsList.matchingItems.length.toString());
            createMap();
        }
    });

    friendsList.filter(function(item) {
        var workDistance = item.values()['work-distance'];
        var homeDistance = item.values()['home-distance'];
        return workDistance >= 0 && workDistance <= 8 && homeDistance >= 0 && homeDistance <= 8;
    });



    function formatTime(timeString) {
        return moment([0, 0, 0, 0, timeString, 0, 0]).format('HH:mm');
    }

    $("#range-hours").slider({
        range: true,
        step: 15,
        min: 360,
        max: 1380,
        values: [900, 1800],
        slide: function(event, ui) {
            var startTime = formatTime(ui.values[0]);
            var endTime = formatTime(ui.values[1]);
            $("#hours-input").val(startTime + '-' + endTime);
        }
    });



    $("#hours-input").val(formatTime($("#range-hours").slider("values", 0)) + " - " +
        formatTime($("#range-hours").slider("values", 1)));





    // function calcRoute() {
    //     var selectedMode = document.getElementById('mode').value;
    //     var request = {
    //         origin: home,
    //         destination: work,
    //         // Note that Javascript allows us to access the constant
    //         // using square brackets and a string value as its
    //         // "property."
    //         travelMode: google.maps.TravelMode[selectedMode]
    //     };
    //     directionsService.route(request, function(response, status) {
    //         if (status == google.maps.DirectionsStatus.OK) {
    //             directionsDisplay.setDirections(response);
    //         }
    //     });
    // };


    // var directionsDisplay;
    // var directionsDisplay2;
    // var directionsDisplay3;
    // var directionsDisplay4;
    // var directionsService = new google.maps.DirectionsService();
    // var map;



    // //The own route to work
    // var home = new google.maps.LatLng(52.355766, 4.948525);
    // var work = new google.maps.LatLng(52.333274, 4.86559);
    // var polylineOptionsActual = {
    //     strokeColor: '#6666FF',
    //     strokeOpacity: 0.8,
    //     strokeWeight: 6
    // };

    // directionsDisplay = new google.maps.DirectionsRenderer({
    //     suppressMarkers: true,
    //     polylineOptions: polylineOptionsActual
    // });
    // directionsDisplay2 = new google.maps.DirectionsRenderer({
    //     suppressMarkers: true,
    //     polylineOptions: polylineOptionsFriends
    // });
    // directionsDisplay3 = new google.maps.DirectionsRenderer({
    //     suppressMarkers: true,
    //     polylineOptions: polylineOptionsFriends
    // });
    // directionsDisplay4 = new google.maps.DirectionsRenderer({
    //     suppressMarkers: true,
    //     polylineOptions: polylineOptionsFriends
    // });



    // var featureOpts = [{
    //     "featureType": "water",
    //     "stylers": [{
    //         "visibility": "on"
    //     }, {
    //         "color": "#666666"
    //     }]
    // }, {
    //     "elementType": "labels",
    //     "stylers": [{
    //         "visibility": "off"
    //     }]
    // }, {
    //     "stylers": [{
    //         "gamma": 4
    //     }]
    // }];


    // //Positions and Route of Friends

    // //Linestyle for friend's routes
    // var polylineOptionsFriends = {
    //     strokeColor: '#666666',
    //     strokeOpacity: 0.8,
    //     strokeWeight: 6
    // };



    // //Friend #1
    // var friend_home1 = new google.maps.LatLng(52.365693, 4.952452);
    // var friend_work1 = new google.maps.LatLng(52.340261, 4.874349);
    // var request = {
    //     origin: friend_home1,
    //     destination: friend_work1,
    //     // Note that Javascript allows us to access the constant
    //     // using square brackets and a string value as its
    //     // "property."
    //     travelMode: 'DRIVING'
    // };
    // directionsService.route(request, function(response, status) {
    //     if (status == google.maps.DirectionsStatus.OK) {
    //         directionsDisplay2.setDirections(response);
    //     }
    // });

    // //Friend #2
    // var friend_home2 = new google.maps.LatLng(52.350780, 4.936702);
    // var friend_work2 = new google.maps.LatLng(52.334611, 4.867038);
    // var request = {
    //     origin: friend_home2,
    //     destination: friend_work2,
    //     // Note that Javascript allows us to access the constant
    //     // using square brackets and a string value as its
    //     // "property."
    //     travelMode: 'BICYCLING'
    // };
    // directionsService.route(request, function(response, status) {
    //     if (status == google.maps.DirectionsStatus.OK) {
    //         directionsDisplay3.setDirections(response);
    //     }
    // });

    // //Friend #3
    // var friend_home3 = new google.maps.LatLng(52.350780, 4.936702);
    // var friend_work3 = new google.maps.LatLng(52.337925, 4.872999);
    // var request = {
    //     origin: friend_home3,
    //     destination: friend_work3,
    //     // Note that Javascript allows us to access the constant
    //     // using square brackets and a string value as its
    //     // "property."
    //     travelMode: 'BICYCLING'
    // };
    // directionsService.route(request, function(response, status) {
    //     if (status == google.maps.DirectionsStatus.OK) {
    //         directionsDisplay4.setDirections(response);
    //     }
    // });

    // // var contentString = '<div class="twitter-message"><img src="images/friend1.jpg"><blockquote class="pull-right"><p>Today I will not go to work!!</p><small>Andre Eddie</small></blockquote></div>';

    // var infowindow1 = new google.maps.InfoWindow({
    //     content: '<div class="twitter-message"><img src="images/friend1.jpg"><blockquote class="pull-right"><p>Today I will not go to work!!</p></blockquote></div>',
    //     disableAutoPan: false
    // });

    // var infowindow2 = new google.maps.InfoWindow({
    //     content: '<div class="twitter-message"><img src="images/friend2.jpg"></div>',
    //     disableAutoPan: false
    // });

    // var infowindow3 = new google.maps.InfoWindow({
    //     content: '<div class="twitter-message"><img src="images/friend3.jpg"></div>',
    //     disableAutoPan: false
    // });



    // function initialize() {


    //     var mapOptions = {
    //         center: new google.maps.LatLng(52.370197, 4.890444),
    //         zoom: 14,
    //         mapTypeControlOptions: {
    //             mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'custom_style']
    //         },
    //         mapTypeId: google.maps.MapTypeId.ROADMAP
    //     };
    //     var map = new google.maps.Map(document.getElementById("map-canvas"),
    //         mapOptions);
    //     directionsDisplay.setMap(map);
    //     directionsDisplay2.setMap(map);
    //     directionsDisplay3.setMap(map);
    //     directionsDisplay4.setMap(map);

    //     var MarkerUser = new google.maps.Marker({
    //         position: work,
    //         map: map,
    //         // icon: 'images/marker_bernd.png'
    //     });

    //     var MarkerFriend1 = new google.maps.Marker({
    //         position: friend_work1,
    //         map: map,
    //         // icon: 'images/marker_friend1.png'
    //     });

    //     var MarkerFriend2 = new google.maps.Marker({
    //         position: friend_work2,
    //         map: map,
    //         // icon: 'images/marker_friend2.png'
    //     });

    //     var MarkerFriend3 = new google.maps.Marker({
    //         position: friend_work3,
    //         map: map,
    //         // icon: 'images/marker_friend2.png'
    //     });

    //     //Open Status Update Twitter
    //     infowindow2.open(map, MarkerFriend2);
    //     infowindow1.open(map, MarkerFriend1);
    //     infowindow3.open(map, MarkerFriend3);

    //     var styledMapOptions = {
    //         name: 'X-Ray'
    //     };

    //     var customMapType = new google.maps.StyledMapType(featureOpts, styledMapOptions);

    //     map.mapTypes.set('custom_style', customMapType);

    //     //Adding Radius around Users work
    //     var radiusWork = {
    //         strokeColor: '#FF0000',
    //         strokeOpacity: 0.3,
    //         strokeWeight: 2,
    //         fillColor: '#FF0000',
    //         fillOpacity: 0.1,
    //         map: map,
    //         center: work,
    //         radius: 1500
    //     };
    //     // Add the circle for this city to the map.
    //     var cityCircle = new google.maps.Circle(radiusWork);

    //     calcRoute();

    // }
    // google.maps.event.addDomListener(window, 'load', initialize);


});
